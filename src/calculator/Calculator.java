/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Tural
 */
public class Calculator {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double a = 0;
        double b = 0;
        String operator = "";
        double result = 0;
        boolean valid = true;
  
        try {           
            BufferedReader in = new BufferedReader(
	    new InputStreamReader(System.in));
            
            System.out.println("Please, enter first number:");       
            a = Double.parseDouble(in.readLine());
            
            System.out.println("Please, enter second number:");
            b = Double.parseDouble(in.readLine());
            
            System.out.println("Please, enter operator:");
            operator = in.readLine();
            
               if(operator.equals("/") && (b != 0))
        {
            valid = false;
            System.out.println("Division by zero.");
        }
             
          switch (operator) {	   
	    case "+":
		result = a+ b;
		break;
	    case "-":
		result = a-b;
		break;
            case "*":
		result = a*b;
		break;
            case "/":
		result = a/b;
		break;
            case "%":
		result = a % b;
		break;     
            default:
                valid = false;
		System.out.println("Invalid operator.");
		break;
	    }
                    
          if (valid){
                      System.out.println("Result: " + a + " " + operator + " " + b + " = " + result);
                }
        }
        catch (NumberFormatException ex)
        {
            System.out.println("Number is incorrect.");       
        }
        catch (IOException ex) {
         //Logger.getLogger(Calculator.class.getName()).log(Level.SEVERE, null, ex);
           
    }
};
    
